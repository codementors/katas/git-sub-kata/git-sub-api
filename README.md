# Git Submodule API

Submodule for the API used as contract between backend and frontend

## Practice

- Try to update this module and have a look to the frontend and backend projects
- Try to update this module in backend and frontend and push back the changes
- Try to use a common workflow (feature branch -> rebase main -> merge back)